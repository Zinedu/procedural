﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Generator : MonoBehaviour
{
    public int seed;
    public int xSize;
    public int ySize;
    float[,] map;
    public float modifier;
    public int deepGroundModifier;

    Tilemap mapGrid;

    // Start is called before the first frame update

    private void Start()
    {
        map = new float[xSize,ySize];
        mapGrid = GameObject.Find("Tilemap").GetComponent<Tilemap>();
        GenMapInner();
        GenMapSurface();
        GenMapDepths();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenMapInner()
    {
        
        for(int x = 0; x < xSize; x++)
        {
            for(int y = 0; y< ySize; y++)
            {
                map[x, y] = Mathf.PerlinNoise(((float)x + seed)  * modifier,((float)y + seed) * modifier);

                if (map[x, y] > 0.2f && map[x, y] < 0.3) {

                    
                    mapGrid.SetTile(new Vector3Int(x, y, 0), (TileBase)Resources.Load("Ground"));
                }
                if (map[x, y] > 0.3f)
                {
                    mapGrid.SetTile(new Vector3Int(x, y, 0), (TileBase)Resources.Load("DeepGround"));
                }

            }
        }

    }

    void GenMapSurface()
    {
        float[] upperMap = new float[xSize];
        string[] upperMapStr = new string[xSize];
        for(int x = 0; x < xSize; x++)
        {
            upperMap[x] = Mathf.PerlinNoise(((float)x + seed + xSize) * modifier, ((float)x + seed + xSize) * modifier);
            upperMapStr[x] = upperMap[x].ToString("F1");
            upperMapStr[x] = upperMapStr[x][2].ToString();
            
            for(int y = 0; y < ySize; y++)
            {
                int target = int.Parse(upperMapStr[x]);
                if (y < target + ySize / 10) {
                        if (mapGrid.GetTile(new Vector3Int(x, (y + ySize) - 1, 0)) && mapGrid.GetTile(new Vector3Int(x, (y + ySize) - 1, 0)).name == "DeepGround" && (target - y) > 2)
                        {
                            mapGrid.SetTile(new Vector3Int(x, y + ySize, 0), (TileBase)Resources.Load("DeepGround"));
                        }
                        else
                        {
                            mapGrid.SetTile(new Vector3Int(x, y + ySize, 0), (TileBase)Resources.Load("Ground"));
                        }                    
                }
                if (y == target + ySize / 10)
                {
                    mapGrid.SetTile(new Vector3Int(x, y + ySize, 0), (TileBase)Resources.Load("Grass"));
                }
            }

        }
        
    }

    void GenMapDepths()
    {
        float[] depthMap = new float[xSize];
        string[] depthMapStr = new string[xSize];
        for(int x = 0; x < xSize; x++)
        {
            depthMap[x] = Mathf.PerlinNoise(((float)x + seed + xSize + deepGroundModifier) * modifier, ((float)x + seed + xSize + deepGroundModifier) * modifier);
            depthMapStr[x] = depthMap[x].ToString("F1");
            depthMapStr[x] = depthMapStr[x][2].ToString();

            for (int y = 0; y < ySize; y++)
            {
                int target = int.Parse(depthMapStr[x]);
                if(y >= ySize - target)
                {
                    mapGrid.SetTile(new Vector3Int(x, y - ySize, 0), (TileBase)Resources.Load("DeepGround"));
                }

                
                if (y <= target) {
                    mapGrid.SetTile(new Vector3Int(x, (y - ySize * 2) / 3, 0), (TileBase)Resources.Load("DeepGround"));
 
                }

            }

        }
    }
}
