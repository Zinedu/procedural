﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralTerrain : MonoBehaviour
{
    GameObject procTerrain;
    TerrainData tData;
    float[,] noiseMap;
    float[,,] terrainHeights;
    int sizeX = 256;
    int sizeY = 256;
    int sizeZ = 256;
    public int seed;
    public float freq;

    // Start is called before the first frame update
    void Start()
    {
        noiseMap = new float[sizeX, sizeY];
        terrainHeights = new float[sizeX, sizeY, sizeZ];


        GenMap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenMap()
    {
        tData = new TerrainData();
        tData.size = new Vector3(sizeX, 50, sizeY);
        tData.heightmapResolution = sizeX + 1;
        procTerrain = Instantiate(Terrain.CreateTerrainGameObject(tData), new Vector3(0, 0, 0), Quaternion.identity);

            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    noiseMap[x, y] = Mathf.PerlinNoise(x + seed * freq, y + seed * freq);


                }
            }
            tData.SetHeights(0, 0, noiseMap);
        }
    
}
